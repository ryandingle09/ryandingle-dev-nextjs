import Fade from 'react-reveal'
import Link from 'next/link'
import Zoom from 'react-reveal/Zoom'
import Moment from 'react-moment'
import 'moment-timezone'

export const Recent = (props) => {

    return(
        <aside className="single_sidebar_widget popular_post_widget">
        {
            <div>
                {
                    props.data.length === 0 ?
                        <div>
                            <Fade right>
                                <h3 className="widget_title">Recent Tutorials</h3>
                            </Fade>
                            <p className="text-center">
                                    <h4>No Recent Tutorial Yet!</h4>
                                </p>
                        </div>
                    :
                    <div>
                        <Fade right>
                            <h3 className="widget_title">Recent Tutorials</h3>
                        </Fade>
                            {
                                props.data.map(item =>
                                    <Zoom>
                                        <div className="media post_item">
                                            <img src={`${process.env.MEDIA_URL}${item.fields.feature_image}`} alt="post" style={{width: '100px', height: '60px'}} />
                                            <div className="media-body">
                                                <Link href={`/tutorial/series/`+item.fields.slug} >
                                                    <a><h3>{item.fields.title}</h3></a>
                                                </Link>
                                                <p><Moment toNow>{item.fields.published_date}</Moment></p>
                                            </div>
                                        </div>
                                    </Zoom>
                                ) 
                            }
                    </div>
                }
            </div>
        }

        <div className="br" />
        </aside>
    )
}

export default Recent