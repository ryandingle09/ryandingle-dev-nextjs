import React, { useState } from 'react'
import { Fade } from 'react-reveal'
import Categories from './categories'
import Recent from './recent'
import Ads from '../../global/ads'

export const Sidebar = (props) =>  {
        
    return (

        <div className="col-lg-4">
            <div className="blog_right_sidebar">
                <aside className="single_sidebar_widget search_widget">
                    <Fade right>
                        <form action="/tutorial">
                            <div className="input-group">
                                <input type="text" name="search" className="form-control" placeholder="Search Tutorial" />
                                <span className="input-group-btn">
                                <button className="btn btn-default" type="button"><i className="lnr lnr-magnifier" /></button>
                                </span>
                            </div>
                        </form>
                    </Fade>
                    <div className="br" />
                </aside>
                
                <Recent item={props.item} data={props.recents} />

                <aside className="single_sidebar_widget">
                    <Ads />
                </aside>

                <Categories isRefer={false} cat="" data={props.categories} />
            </div>
        </div>
    )
}

export default Sidebar