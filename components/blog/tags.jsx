import React from 'react'
import Fade from 'react-reveal'
import Link from 'next/link'

export const Tags = (props) => {
    return(
        <div>
            {
                props.isRefer ?
                    props.items.map(item =>
                        props.data.map(item2 =>
                            item2.pk === item ?
                                <Link href={`/blog/tag/${item2.pk}`}>
                                    <a><span className="badge badge-primary">{item2.fields.title}</span>&nbsp;</a>
                                </Link>
                            : null
                        )
                    )
                : 
                <div>
                    <Fade bottom>
                        <aside className="single-sidebar-widget tag_cloud_widget">
                            <h4 className="widget_title">Tag Clouds</h4>
                            {
                                props.data.length === 0 ?
                                    <p className="text-center">
                                        <h4>No Tags Yet!</h4>
                                    </p>
                                :
                                    <ul className="list">
                                        {
                                            props.data.map(item =>
                                                <li>
                                                    <Link href={`/blog/tag/`+item.pk }>
                                                        <a>{item.fields.title}</a>
                                                    </Link>
                                                </li>
                                            ) 
                                        }
                                    </ul>
                            }
                        </aside>
                    </Fade>
                </div>
            }
        </div>
    )
}

export default Tags