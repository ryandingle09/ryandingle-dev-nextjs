import React, { useState } from 'react'
import { Fade } from 'react-reveal'
import Categories from './categories'
import Tags from './tags'
import Recent from './recent'
import Ads from '../../global/ads'

export const Sidebar = (props) =>  {
        
    return (

        <div className="col-lg-4">
            <div className="blog_right_sidebar">
                <aside className="single_sidebar_widget search_widget">
                    <Fade right>
                        <form action="/blog">
                            <div className="input-group">
                                <input type="text" name="search" className="form-control" placeholder="Search Posts" />
                                <span className="input-group-btn">
                                <button className="btn btn-default" type="button"><i className="lnr lnr-magnifier" /></button>
                                </span>
                            </div>
                        </form>
                    </Fade>
                    <div className="br" />
                </aside>
                
                <Recent item={props.item} data={props.recents} />

                <aside className="single_sidebar_widget">
                    <Ads />
                </aside>

                <Categories isRefer={false} cat="" data={props.categories} />
                
                {/* <Fade bottom>
                    <aside className="single-sidebar-widget newsletter_widget">
                        <h4 className="widget_title">Newsletter</h4>
                        <p>
                            Here, I focus on a range of items and features that we use in life without
                            giving them a second thought.
                        </p>
                        <div className="form-group d-flex flex-row">
                            <div className="input-group">
                            <div className="input-group-prepend">
                                <div className="input-group-text"><i className="fa fa-envelope" aria-hidden="true" /></div>
                            </div>
                            <input type="text" className="form-control" id="inlineFormInputGroup" placeholder="Enter email" />
                            </div>
                            <Link href="/"><a className="bbtns">Subcribe</a></Link> 
                        </div>
                        <p className="text-bottom">You can unsubscribe at any time</p>
                        <div className="br" />
                    </aside>
                </Fade> */}
                
                <Tags isRefer={false} items={[]} data={props.tags} />
            </div>
        </div>
    )
}

export default Sidebar