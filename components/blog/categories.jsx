import React from 'react'
import Fade from 'react-reveal'
import Link from 'next/link'
import Bounce from 'react-reveal/Bounce'

export const Categories = (props) =>{
    return(
        <div>
            {
                props.isRefer ?
                    props.data.map(item2 =>
                        item2.pk === props.cat ?
                            <Link href={`/blog/category/${item2.pk}`}>
                                <a>{item2.fields.title}&nbsp;<i className="fa fa-folder-o" /></a>
                            </Link>
                        : null
                    )
                : 
                    <div>
                        <Fade bottom>
                            <aside className="single_sidebar_widget post_category_widget">
                                <h4 className="widget_title">Post Categories</h4>
                                    {
                                        props.data.length === 0 ?
                                        <p className="text-center">
                                            <h4>No Categories Yet!</h4>
                                        </p>
                                        :
                                        <ul className="list cat-list">
                                            {
                                                props.data.map(item =>
                                                    <li>
                                                        <Bounce>
                                                            <Link href={`/blog/category/${item.pk}`}>
                                                                <a className="d-flex justify-content-between">
                                                                    <p>{item.fields.title}</p>
                                                                    <p>{item.post_count}</p>
                                                                </a>
                                                            </Link>
                                                        </Bounce>
                                                    </li>
                                                ) 
                                            }
                                        </ul>
    
                                    }
                                    
                                <div className="br" />
                            </aside>
                        </Fade>
                    </div>
            }
        </div>
    )
}
  
export default Categories