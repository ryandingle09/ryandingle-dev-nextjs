import React from 'react'

export const Preloader = () => {
    return (
        <div className="section_gap container">
            <div className="row">
                <div className="col-md-8">
                    <div className="br">
                        <div className="wrapper">
                            <div className="profilePic animate din"></div>
                            <div className="comment br animate w80"></div>
                            <div className="comment br animate"></div>
                            <div className="comment br animate"></div>

                            <br />
                            <br />

                            <div className="comment br animate w80"></div>
                            <div className="comment br animate"></div>
                            <div className="comment br animate"></div>

                            <div className="comment br animate w80"></div>
                            <div className="comment br animate"></div>
                            <div className="comment br animate"></div>

                            <br />
                            <br />

                            <div className="comment br animate w80"></div>
                            <div className="comment br animate"></div>
                            <div className="comment br animate"></div>

                            <div className="comment br animate w80"></div>
                            <div className="comment br animate"></div>
                            <div className="comment br animate"></div>
                        </div>
                    </div>
                </div>
                <div className="col-md-4">
                    <div className="br">
                        <div className="wrapper">
                            <div className="comment br animate w80"></div>
                            <div className="comment br animate"></div>
                            <div className="comment br animate"></div>

                            <br />
                            <br />

                            <div className="comment br animate w80"></div>
                            <div className="comment br animate"></div>
                            <div className="comment br animate"></div>

                            <div className="comment br animate w80"></div>
                            <div className="comment br animate"></div>
                            <div className="comment br animate"></div>

                            <br />
                            <br />

                            <div className="comment br animate w80"></div>
                            <div className="comment br animate"></div>
                            <div className="comment br animate"></div>

                            <div className="comment br animate w80"></div>
                            <div className="comment br animate"></div>
                            <div className="comment br animate"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Preloader

