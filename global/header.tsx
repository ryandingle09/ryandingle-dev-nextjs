import Link from 'next/link'
import { Flip, LightSpeed } from 'react-reveal'

export const Header = (props) => {
    return (
        <header className="header_area navbar_fixed">
            <div className="main_menu">
                <nav className="navbar navcontainer navbar-expand-lg navbar-light">

                    <Flip top>
                        <div className="container">
                            <Link href="/">
                                <a className="navbar-brand" href="/">
                                {
                                    props.Site.length === 0 ?
                                            <img src="/img/logo-top.png" alt="..." />
                                    :
                                    props.Site.map(item => 
                                        item.fields.use_logo ?
                                            <img src={`${process.env.MEDIA_URL}${item.fields.logo}`} alt={item.fields.title} />
                                        :
                                            <h2 style={{color: '#007bff'}}>{item.fields.title}</h2>
                                    )
                                }
                                </a>
                            </Link>
                            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span className="icon-bar" />
                                <span className="icon-bar" />
                                <span className="icon-bar" />
                            </button>
                            <div className="collapse navbar-collapse offset" id="navbarSupportedContent">
                                <LightSpeed right>
                                <ul className="nav navbar-nav menu_nav justify-content-end">
                                    <li className="nav-item active">
                                        <Link href="/"><a href="/"className="nav-link">Home</a></Link>
                                    </li>
                                    <li className="nav-item active">
                                        <Link href="/about"><a href="/about"className="nav-link">About</a></Link>
                                    </li>
                                    <li className="nav-item active">
                                        <Link href="/portfolio"><a href="/portfolio"className="nav-link">Portfolio</a></Link>
                                    </li>
                                    <li className="nav-item active">
                                        <Link href="/tutorial"><a href="/tutorial"className="nav-link">Tutorials</a></Link>
                                    </li>
                                    <li className="nav-item active">
                                        <Link href="/services" ><a href="/services"className="nav-link">Services</a></Link>
                                    </li>
                                    <li className="nav-item active">
                                        <Link href="/blog" ><a href="/blog"className="nav-link">Blog</a></Link>
                                    </li>
                                    <li className="nav-item active">
                                        <Link href="/contact"><a href="/contact"className="nav-link">Contact</a></Link>
                                    </li>
                                </ul>
                                </LightSpeed>
                            </div>
                        </div>
                    </Flip>

                </nav>
            </div>
        </header>
    )
}

export async function getInitialProps() {
  
    const res = await fetch(`${process.env.API_URL}site`)
    const Site = await res.json()
  
    const res2 = await fetch(`${process.env.API_URL}social`)
    const Social = await res2.json()
  
    return {  Social, Site }
}

export default Header