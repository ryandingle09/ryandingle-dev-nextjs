import React from 'react'
import Head from 'next/head'

export const Preloader2 = () => {
    return (
        <div className="section_gap container">

            <Head>
                <title>Loading Please Wait ...</title>
            </Head>

            <div className="br">
                <div className="wrapper">
                    <div className="comment br animate w80"></div>
                    <div className="comment br animate"></div>
                    <div className="comment br animate"></div>

                    <br />
                    <br />

                    <div className="comment br animate w80"></div>
                    <div className="comment br animate"></div>
                    <div className="comment br animate"></div>

                    <div className="comment br animate w80"></div>
                    <div className="comment br animate"></div>
                    <div className="comment br animate"></div>

                    <br />
                    <br />

                    <div className="comment br animate w80"></div>
                    <div className="comment br animate"></div>
                    <div className="comment br animate"></div>

                    <div className="comment br animate w80"></div>
                    <div className="comment br animate"></div>
                    <div className="comment br animate"></div>
                </div>
            </div>
        </div>
    );
}

export default Preloader2

