import React from 'react'
// import { graphql } from "gatsby-graphql";
import { DiscussionEmbed } from "disqus-react"

export const DisqusComments = (props) => {
    const config = { 
        identifier: props.item.fields.slug,
        title: props.item.fields.title,
        url: `${process.env.APP_URL}blog/post/${props.item.fields.slug}`
    }
    
    return (
        <DiscussionEmbed shortname={process.env.DISQUS_NAMESPACE} config={config}  />
    );
};

export default DisqusComments