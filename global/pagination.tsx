import Link from 'next/link'

export const Pagination = (props) => {

    let arr = Array.apply(null, {length: props.pages}).map(Number.call, Number)
    let pages = props.pages
    let active = props.active
    let prev = (parseInt(active) - 1)
    let next = (parseInt(active) + 1)

    return (    

        <nav className="blog-pagination justify-content-center d-flex">
            {
                pages === 1 ? null
                :
                <ul className="pagination">

                    {
                        active !== 1 ?
                            <li className="page-item">
                                <Link href={`/${props.path}?page=${prev}`}>
                                    <a className="page-link" aria-label="Previous">
                                        <span aria-hidden="true">
                                            <span className="lnr lnr-chevron-left" /> Previous
                                        </span>
                                    </a>
                                </Link>
                            </li>
                        : null
                    }

                    {arr.map(item => {
                        if((item+1) === active) {
                            return (
                                // <li className="page-item active">
                                <li className="page-item">
                                    <Link href={`/${props.path}?page=${(item + 1)}`}>
                                        <a className="page-link">
                                        {(item+1)}
                                        </a>
                                    </Link>
                                </li>
                            )
                        }else{
                            return (
                                <li className="page-item">
                                    <Link href={`/${props.path}?page=${(item + 1)}`}>
                                        <a className="page-link">
                                        {(item+1)}
                                        </a>
                                    </Link>
                                </li>
                            )
                        }
                    })}

                    {
                        active !== pages?
                            <li className="page-item">
                                <Link href={`/${props.path}?page=${next}`}>
                                    <a className="page-link" aria-label="Next">
                                        <span aria-hidden="true">
                                            Next <span className="lnr lnr-chevron-right" />
                                        </span>
                                    </a>
                                </Link>
                            </li>
                        : null
                    }

                </ul>
            }
        </nav>
    )
}

export default Pagination;