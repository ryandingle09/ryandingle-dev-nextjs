import Head from 'next/head'
import Header from './header'
import Footer from './footer'
import { ToastContainer, toast } from 'react-toastify'

export const Layout = props => {
  return (
    <div>
      
      <Head>
        <script data-ad-client={process.env.ADSENSE_ID} async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <meta property="fb:app_id" content="265900324300424" />
        <meta name="twitter:site" content="@ryandingle09" />

        <link rel="shortcut icon" type="image/png" href="/ico.png" />

        <link rel="stylesheet" href="/css/font-awesome.min.css" />
        <link rel="stylesheet" href="/vendors/owl-carousel/owl.carousel.min.css" />
        <link rel="stylesheet" href="/css/magnific-popup.css" />
        <link rel="stylesheet" href="/css/style.css" />

        <script src="/js/jquery-3.2.1.min.js"></script>
        <script src="/js/popper.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/stellar.js"></script>
        <script src="/js/jquery.magnific-popup.min.js"></script>
        <script src="/vendors/nice-select/js/jquery.nice-select.min.js"></script>
        <script src="/vendors/isotope/imagesloaded.pkgd.min.js"></script>
        <script src="/vendors/isotope/isotope-min.js"></script>
        <script src="/vendors/owl-carousel/owl.carousel.min.js"></script>
        <script src="/js/jquery.ajaxchimp.min.js"></script>
        <script src="/js/mail-script.js"></script>
        <script src="/js/theme.js"></script>
      </Head>

      <ToastContainer position={toast.POSITION.BOTTOM_CENTER} autoClose={false} />

      <Header {...props} />
      
      <main>{props.children}</main>

      <Footer {...props} />
    </div>
  )
}

export default Layout
