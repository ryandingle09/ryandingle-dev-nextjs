
import AdSense from 'react-adsense'

export const Ads = () =>  {
        
    return (

        <AdSense.Google
            client={process.env.ADSENSE_ID}
            //slot='7806394673'
            style={{ display: 'block' }}
            format='auto'
            responsive='true'
            layoutKey='-gw-1+2a-9x+5c'
        />
    )
}

export default Ads