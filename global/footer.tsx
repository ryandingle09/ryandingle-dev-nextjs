import Social from '../global/social';
import { Bounce } from 'react-reveal';

export const Footer = (props) =>  {
    return (
        <footer className="footer_area section_gap">
            <Bounce>
            <div className="container">
                <div className="row footer_inner justify-content-center">
                    {
                        props.Site.length === 0 ?
                            <div className="col-lg-6 text-center">
                                <aside className="f_widget social_widget">
                                    <div className="f_logo">
                                        <img src="/img/logo.png" alt="" />
                                    </div>
                                    <div className="f_title">
                                        <h4>Follow Me</h4>
                                    </div>
                                    <Social social={props.Social} />
                                </aside>
                                <div className="copyright">
                                    <p>
                                        Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i className="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://rdingle.com" target="_blank" rel="noopener noreferrer">RLD WEBSHOP</a>
                                    </p>
                                </div>
                            </div>
                        :
                        props.Site.map(item => 
                            <div className="col-lg-6 text-center">
                                <aside className="f_widget social_widget">
                                    <div className="f_logo">
                                        {
                                            item.fields.use_logo ?
                                            <img src={`${process.env.MEDIA_URL}${item.fields.logo}`} alt={item.fields.title} />
                                            :
                                            <h1 style={{color: 'white'}}>{item.fields.title}</h1>
                                        }
                                    </div>
                                    <div className="f_title">
                                        <h4>Follow Me</h4>
                                    </div>
                                    <Social social={props.Social} />
                                </aside>
                                <div className="copyright">
                                    <p>
                                        Copyright &copy;{item.fields.year} All rights reserved <i className="fa fa-star-o" aria-hidden="true"></i> by <a href={`https://`+item.fields.domain_name} target="_blank" rel="noopener noreferrer">{item.fields.title}</a>
                                    </p>
                                </div>
                            </div>
                        )
                    }
                </div>
            </div>
            </Bounce>
        </footer>
    )
}


export async function getInitialProps() {
  
    const res = await fetch(`${process.env.API_URL}site`)
    const Site = await res.json()
  
    const res2 = await fetch(`${process.env.API_URL}social`)
    const Social = await res2.json()
  
    return {  Social, Site }
}

export default Footer
