import fetch from 'node-fetch'
import Head from 'next/head'
import Link from 'next/link'
import { Fade, Zoom, LightSpeed, Flip, Bounce } from 'react-reveal'
import RubberBand from 'react-reveal/RubberBand'

export const siteTitle = 'Ryan Dingle | A Man From Outer Space :D'

export const Home = (props) => {
  return (
      <>
        <Head>
            <title>{siteTitle}</title>
            <meta property="og:title" content={siteTitle}  />
            <meta property="og:type" content="article" />
            <meta property="og:description" content={props.Site[0]?.fields.description} />
            <meta property="og:image" content={ `${process.env.MEDIA_URL}${props.Site[0]?.fields.image}` } />
            <meta property="og:url" content={ process.env.APP_URL } />
            <meta name="twitter:card" content={ `${process.env.MEDIA_URL}${props.Site[0]?.fields.image}` } />
    
            <meta property="og:site_name" content={siteTitle} />
            <meta name="twitter:image:alt" content={siteTitle} />
          </Head>

          {
            props.Site.length === 0  ? 
              <Zoom>
                <section className="banner-area" id="home" style={{backgroundImage: 'url(/img/bg3.jpg)', backgroundSize: 'cover', height: '100vh' }}>
                  <div className="single_slide_banner">
                    <div className="container">
                      <div className="row fullscreen d-flex align-items-center">
                        <div className="banner-content col-lg-12 justify-content-center" style={{textAlign: 'center'}}>
                          <RubberBand cascade collapse delay={1000}><h1>{props.Site[0]?.fields.header_title}</h1></RubberBand>
                          <Zoom opposite cascade collapse delay={1200}><h3>{props.Site[0]?.fields.header_description}</h3></Zoom>
                          <Fade delay={1500}><Link href="/about"><a href="/about" className="primary-btn">Know more about me ?</a></Link></Fade>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
              </Zoom>
            :
              props.Site.map(item => 
                <Zoom>
                  <section className="banner-area" id="home" style={{backgroundImage: "url(" + process.env.MEDIA_URL +``+ item.fields.background + ")", backgroundSize: 'cover', height: '100vh' }}>
                    <div className="single_slide_banner">
                      <div className="container">
                        <div className="row fullscreen d-flex align-items-center">
                          <div className="banner-content col-lg-12 justify-content-center" style={{textAlign: 'center'}}>
                            <RubberBand cascade collapse delay={1000}><h1>{item.fields.tagline}</h1></RubberBand>
                            <Zoom opposite cascade collapse delay={1200}><h3>{item.fields.heading}</h3></Zoom>
                            <Fade delay={1500}><Link href="/about"><a href="/about" className="primary-btn">Know more about me ?</a></Link></Fade>
                          </div>
                        </div>
                      </div>
                    </div>
                  </section>
                </Zoom>
              )

          }

          <Fade bottom>
            <section className="section_gap portfolio_area" id="work">

              <div className="container">

                <div className="row justify-content-center">
                  <div className="col-lg-6 text-center">
                    <div className="main-title">
                      <h1>Recent Works</h1>
                    </div>
                  </div>
                </div>

                <div className="row justify-content-center">
                    {
                      props.WorkHome.length === 0  ? 
                      <div className="card-columns">
                        <Zoom>
                          <div className="card bg-light text-white items" data-item="1" onMouseEnter={this.handleMouseHover} onMouseLeave={this.handleMouseHover}>
                            <Link href="/blog/item">
                              <a href="/blog/item" className="plain-link2">
                                <img src="/img/saitama.jpg" className="card-img" alt="/img/saitama.jpg" style={{width: '366px', height: '366px'}} />
                                <div className="card-img-overlay text-center">
                                  {
                                    this.state.isHovering && this.state.isItemHover === 1
                                    ?
                                    <div>
                                      <Flip right cascade when={this.state.toggleHoverState}>
                                        <h3 className="card-title white-shadow title">This is a Sample Title</h3>
                                      </Flip>
                                      <Flip bottom cascade when={this.state.show}>
                                        <a href="/blog/item" className="primary-btn white-color white-shadow">View</a>
                                      </Flip>
                                    </div>
                                    : ''
                                  }
                                </div>
                              </a>
                            </Link>
                          </div>
                        </Zoom>
                      
                        <Zoom>
                          <div className="card bg-light text-white items" data-item="1" onMouseEnter={this.handleMouseHover} onMouseLeave={this.handleMouseHover}>
                            <Link href="/blog/item">
                              <a href="/blog/item" className="plain-link2">
                                <img src="/img/saitama.jpg" className="card-img" alt="/img/saitama.jpg" style={{width: '366px', height: '366px'}} />
                                <div className="card-img-overlay text-center">
                                  {
                                    this.state.isHovering && this.state.isItemHover === 1
                                    ?
                                    <div>
                                      <Flip right cascade when={this.state.toggleHoverState}>
                                        <h3 className="card-title white-shadow title">This is a Sample Title</h3>
                                      </Flip>
                                      <Flip bottom cascade when={this.state.show}>
                                        <a href="/blog/item" className="primary-btn white-color white-shadow">View</a>
                                      </Flip>
                                    </div>
                                    : ''
                                  }
                                </div>
                              </a>
                            </Link>
                          </div>
                        </Zoom>

                        <Zoom>
                          <div className="card bg-light text-white items" data-item="1" onMouseEnter={this.handleMouseHover} onMouseLeave={this.handleMouseHover}>
                            <Link href="/blog/item">
                              <a href="/blog/item" className="plain-link2">
                                <img src="/img/saitama.jpg" className="card-img" alt="/img/saitama.jpg" style={{width: '366px', height: '366px'}} />
                                <div className="card-img-overlay text-center">
                                  {
                                    this.state.isHovering && this.state.isItemHover === 1
                                    ?
                                    <div>
                                      <Flip right cascade when={this.state.toggleHoverState}>
                                        <h3 className="card-title white-shadow title">This is a Sample Title</h3>
                                      </Flip>
                                      <Flip bottom cascade when={this.state.show}>
                                        <a href="/blog/item" className="primary-btn white-color white-shadow">View</a>
                                      </Flip>
                                    </div>
                                    : ''
                                  }
                                </div>
                              </a>
                            </Link>
                          </div>
                        </Zoom>

                        <Zoom>
                          <div className="card bg-light text-white items" data-item="1" onMouseEnter={this.handleMouseHover} onMouseLeave={this.handleMouseHover}>
                            <Link href="/blog/item">
                              <a href="/blog/item" className="plain-link2">
                                <img src="/img/saitama.jpg" className="card-img" alt="/img/saitama.jpg" style={{width: '366px', height: '366px'}} />
                                <div className="card-img-overlay text-center">
                                  {
                                    this.state.isHovering && this.state.isItemHover === 1
                                    ?
                                    <div>
                                      <Flip right cascade when={this.state.toggleHoverState}>
                                        <h3 className="card-title white-shadow title">This is a Sample Title</h3>
                                      </Flip>
                                      <Flip bottom cascade when={this.state.show}>
                                        <a href="/blog/item" className="primary-btn white-color white-shadow">View</a>
                                      </Flip>
                                    </div>
                                    : ''
                                  }
                                </div>
                              </a>
                            </Link>
                          </div>
                        </Zoom>

                        <Zoom>
                          <div className="card bg-light text-white items" data-item="1" onMouseEnter={this.handleMouseHover} onMouseLeave={this.handleMouseHover}>
                            <Link href="/blog/item">
                              <a href="/blog/item" className="plain-link2">
                                <img src="/img/saitama.jpg" className="card-img" alt="/img/saitama.jpg" style={{width: '366px', height: '366px'}} />
                                <div className="card-img-overlay text-center">
                                  {
                                    this.state.isHovering && this.state.isItemHover === 1
                                    ?
                                    <div>
                                      <Flip right cascade when={this.state.toggleHoverState}>
                                        <h3 className="card-title white-shadow title">This is a Sample Title</h3>
                                      </Flip>
                                      <Flip bottom cascade when={this.state.show}>
                                        <a href="/blog/item" className="primary-btn white-color white-shadow">View</a>
                                      </Flip>
                                    </div>
                                    : ''
                                  }
                                </div>
                              </a>
                            </Link>
                          </div>
                        </Zoom>

                        <Zoom>
                          <div className="card bg-light text-white items" data-item="1" onMouseEnter={this.handleMouseHover} onMouseLeave={this.handleMouseHover}>
                            <Link href="/blog/item">
                              <a href="/blog/item" className="plain-link2">
                                <img src="/img/saitama.jpg" className="card-img" alt="/img/saitama.jpg" style={{width: '366px', height: '366px'}} />
                                <div className="card-img-overlay text-center">
                                  {
                                    this.state.isHovering && this.state.isItemHover === 1
                                    ?
                                    <div>
                                      <Flip right cascade when={this.state.toggleHoverState}>
                                        <h3 className="card-title white-shadow title">This is a Sample Title</h3>
                                      </Flip>
                                      <Flip bottom cascade when={this.state.show}>
                                        <a href="/blog/item" className="primary-btn white-color white-shadow">View</a>
                                      </Flip>
                                    </div>
                                    : ''
                                  }
                                </div>
                              </a>
                            </Link>
                          </div>
                        </Zoom>
                                  
                      </div>
                      :

                      <div className="card-columns">
                      {
                        props.WorkHome.map(item => 
                          <Zoom>
                            <div className="card bg-light text-white items" data-item={item.pk}>
                              <Link href={`/portfolio/${item.pk}`}>
                                <a href={`/portfolio/${item.pk}`} className="plain-link2">
                                  <img src={process.env.MEDIA_URL+ `` + item.fields.cover_image} className="card-img-top" alt={item.fields.title}  style={{width: '366px', height: '366px'}} />
                                  <div className="card-img-overlay text-center">
                                      <div>
                                        <Flip bottom cascade>
                                          <Link href={'/portfolio/'+ item.pk}>
                                            <a href={'/portfolio/'+ item.pk} className="primary-btn white-color white-shadow" style={{color:'white', marginTop: '40%'}}>
                                              View
                                            </a>
                                          </Link>
                                        </Flip>
                                      </div>
                                  </div>
                                </a>
                              </Link>
                            </div>
                          </Zoom>
                        )
                      }
                      </div>
                    }
                </div>

              </div>
                  
              <div className="container text-center" style={{marginTop:'80px'}}>
                <Link href="/portfolio"><a href="/portfolio" className="primary-btn">Show More</a></Link>
              </div>
            </section>
          </Fade>

          <hr />

          <Fade bottom>
            <section className="section_gap portfolio_area" id="work">

              <div className="container">

                <div className="row justify-content-center">
                  <div className="col-lg-6 text-center">
                    <div className="main-title">
                      <h1>Recent Tutorials</h1>
                    </div>
                  </div>
                </div>

                <div className="row justify-content-center">
                    {
                      props.TutorialHomeRecent.length === 0  ? 
                      <div className="card-columns">
                        <Zoom>
                          <div className="card bg-light text-white items" data-item="1" onMouseEnter={this.handleMouseHover} onMouseLeave={this.handleMouseHover}>
                            <Link href="/blog/item">
                              <a href="/blog/item" className="plain-link2">
                                <img src="/img/saitama.jpg" className="card-img" alt="/img/saitama.jpg" style={{width: '366px', height: '366px'}} />
                                <div className="card-img-overlay text-center">
                                  {
                                    this.state.isHovering && this.state.isItemHover === 1
                                    ?
                                    <div>
                                      <Flip right cascade when={this.state.toggleHoverState}>
                                        <h3 className="card-title white-shadow title">This is a Sample Title</h3>
                                      </Flip>
                                      <Flip bottom cascade when={this.state.show}>
                                        <a href="/blog/item" className="primary-btn white-color white-shadow">View</a>
                                      </Flip>
                                    </div>
                                    : ''
                                  }
                                </div>
                              </a>
                            </Link>
                          </div>
                        </Zoom>
                      
                        <Zoom>
                          <div className="card bg-light text-white items" data-item="1" onMouseEnter={this.handleMouseHover} onMouseLeave={this.handleMouseHover}>
                            <Link href="/blog/item">
                              <a href="/blog/item" className="plain-link2">
                                <img src="/img/saitama.jpg" className="card-img" alt="/img/saitama.jpg" style={{width: '366px', height: '366px'}} />
                                <div className="card-img-overlay text-center">
                                  {
                                    this.state.isHovering && this.state.isItemHover === 1
                                    ?
                                    <div>
                                      <Flip right cascade when={this.state.toggleHoverState}>
                                        <h3 className="card-title white-shadow title">This is a Sample Title</h3>
                                      </Flip>
                                      <Flip bottom cascade when={this.state.show}>
                                        <a href="/blog/item" className="primary-btn white-color white-shadow">View</a>
                                      </Flip>
                                    </div>
                                    : ''
                                  }
                                </div>
                              </a>
                            </Link>
                          </div>
                        </Zoom>

                        <Zoom>
                          <div className="card bg-light text-white items" data-item="1" onMouseEnter={this.handleMouseHover} onMouseLeave={this.handleMouseHover}>
                            <Link href="/blog/item">
                              <a href="/blog/item" className="plain-link2">
                                <img src="/img/saitama.jpg" className="card-img" alt="/img/saitama.jpg" style={{width: '366px', height: '366px'}} />
                                <div className="card-img-overlay text-center">
                                  {
                                    this.state.isHovering && this.state.isItemHover === 1
                                    ?
                                    <div>
                                      <Flip right cascade when={this.state.toggleHoverState}>
                                        <h3 className="card-title white-shadow title">This is a Sample Title</h3>
                                      </Flip>
                                      <Flip bottom cascade when={this.state.show}>
                                        <a href="/blog/item" className="primary-btn white-color white-shadow">View</a>
                                      </Flip>
                                    </div>
                                    : ''
                                  }
                                </div>
                              </a>
                            </Link>
                          </div>
                        </Zoom>

                        <Zoom>
                          <div className="card bg-light text-white items" data-item="1" onMouseEnter={this.handleMouseHover} onMouseLeave={this.handleMouseHover}>
                            <Link href="/blog/item">
                              <a href="/blog/item" className="plain-link2">
                                <img src="/img/saitama.jpg" className="card-img" alt="/img/saitama.jpg" style={{width: '366px', height: '366px'}} />
                                <div className="card-img-overlay text-center">
                                  {
                                    this.state.isHovering && this.state.isItemHover === 1
                                    ?
                                    <div>
                                      <Flip right cascade when={this.state.toggleHoverState}>
                                        <h3 className="card-title white-shadow title">This is a Sample Title</h3>
                                      </Flip>
                                      <Flip bottom cascade when={this.state.show}>
                                        <a href="/blog/item" className="primary-btn white-color white-shadow">View</a>
                                      </Flip>
                                    </div>
                                    : ''
                                  }
                                </div>
                              </a>
                            </Link>
                          </div>
                        </Zoom>

                        <Zoom>
                          <div className="card bg-light text-white items" data-item="1" onMouseEnter={this.handleMouseHover} onMouseLeave={this.handleMouseHover}>
                            <Link href="/blog/item">
                              <a href="/blog/item" className="plain-link2">
                                <img src="/img/saitama.jpg" className="card-img" alt="/img/saitama.jpg" style={{width: '366px', height: '366px'}} />
                                <div className="card-img-overlay text-center">
                                  {
                                    this.state.isHovering && this.state.isItemHover === 1
                                    ?
                                    <div>
                                      <Flip right cascade when={this.state.toggleHoverState}>
                                        <h3 className="card-title white-shadow title">This is a Sample Title</h3>
                                      </Flip>
                                      <Flip bottom cascade when={this.state.show}>
                                        <a href="/blog/item" className="primary-btn white-color white-shadow">View</a>
                                      </Flip>
                                    </div>
                                    : ''
                                  }
                                </div>
                              </a>
                            </Link>
                          </div>
                        </Zoom>

                        <Zoom>
                          <div className="card bg-light text-white items" data-item="1" onMouseEnter={this.handleMouseHover} onMouseLeave={this.handleMouseHover}>
                            <Link href="/blog/item">
                              <a href="/blog/item" className="plain-link2">
                                <img src="/img/saitama.jpg" className="card-img" alt="/img/saitama.jpg" style={{width: '366px', height: '366px'}} />
                                <div className="card-img-overlay text-center">
                                  {
                                    this.state.isHovering && this.state.isItemHover === 1
                                    ?
                                    <div>
                                      <Flip right cascade when={this.state.toggleHoverState}>
                                        <h3 className="card-title white-shadow title">This is a Sample Title</h3>
                                      </Flip>
                                      <Flip bottom cascade when={this.state.show}>
                                        <a href="/blog/item" className="primary-btn white-color white-shadow">View</a>
                                      </Flip>
                                    </div>
                                    : ''
                                  }
                                </div>
                              </a>
                            </Link>
                          </div>
                        </Zoom>
                                  
                      </div>
                      :

                      <div className="card-columns">
                      {
                        props.TutorialHomeRecent.map(item => 
                          <Zoom>
                            <div className="card bg-light text-white items" data-item={item.pk}>
                              <Link href={`/tutorial/series/${item.fields.slug}`}>
                                <a href={`/tutorial/series/${item.fields.slug}`} className="plain-link2">
                                  <img src={process.env.MEDIA_URL+ `` + item.fields.feature_image} className="card-img-top" alt={item.fields.title}  style={{width: '366px', height: '366px'}} />
                                  <div className="card-img-overlay text-center">
                                      <div>
                                        <Flip bottom cascade>
                                          <Link href={'/tutorial/series/'+ item.fields.slug}>
                                            <a href={'/tutorial/series/'+ item.fields.slug} className="primary-btn white-color white-shadow" style={{color:'white', marginTop: '40%'}}>
                                              View
                                            </a>
                                          </Link>
                                        </Flip>
                                      </div>
                                  </div>
                                </a>
                              </Link>
                            </div>
                          </Zoom>
                        )
                      }
                      </div>
                    }
                </div>

              </div>
                  
              <div className="container text-center" style={{marginTop:'80px'}}>
                <Link href="/tutorial"><a href="/tutorial" className="primary-btn">Show More</a></Link>
              </div>
            </section>
          </Fade>

          <LightSpeed left opposite>
            <section className="about-area section_gap gray-bg">
              <div className="container">
                {
                  props.AboutInfo.length === 0 ?
                  <div className="row align-items-center justify-content-between">
                    <div className="col-lg-5 about-left">
                      <img className="img-fluid" src="/img/about-img.png" alt="" />
                    </div>
                    <div className="col-lg-6 col-md-12 about-right">
                      <div className="main-title text-left">
                        <h1>About Me</h1>
                      </div>
                      <div className="mb-50 wow fadeIn" data-wow-duration=".8s">
                        <p>
                          inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct standards
                          especially in the
                          workplace. That’s why it’s crucial that, as women, our behavior on the job is beyond reproach. inappropriate
                          behavior
                          is often laughed. inappropriate behavior is often laughed off as “boys will be boys,” women face higher.
                        </p>
                        <p>That’s why it’s crucial that, as women, our behavior on the job is beyond reproach. inappropriate behavior is
                          often
                          laughed.
                        </p>
                      </div>
                      <Link href="/about"><a href="/about" className="primary-btn">More Info</a></Link>
                    </div>
                  </div>
                  :
                  props.AboutInfo.map(item => 
                    <div className="row align-items-center justify-content-between">
                      <div className="col-lg-5 about-left">
                        <img className="img-fluid" src={process.env.MEDIA_URL+ `` + item.fields.image} alt={item.fields.heading} />
                      </div>
                      <div className="col-lg-6 col-md-12 about-right">
                        <div className="main-title text-left">
                          <h1>About Me</h1>
                        </div>
                        <div className="mb-50 wow fadeIn" data-wow-duration=".8s">
                          <p>{item.fields.introduction}</p>
                        </div>
                      <Link href="/about"><a href="/about" className="primary-btn">More Info</a></Link>
                      </div>
                    </div>
                  )
                }
              </div>
            </section>
          </LightSpeed>

          <LightSpeed right opposite>
            <div className="section_gap testimonial_area" style={{backgroundImage: "url(/img/testi-bg.jpg)", backgroundSize: 'cover' }}>
              <div className="container">
                <div className="row justify-content-center">
                  <div className="col-lg-6 text-center">
                    <div className="main-title">
                      <h1>Recent Blog Posts</h1>
                    </div>
                  </div>
                </div>

                <div className="row justify-content-center">
                  {
                    props.BlogHomeRecent.length === 0 ?
                    <div className="card-columns">
                      <Fade>
                        <div className="text-left article">
                          <img src="/img/saitama.jpg" className="card-img-top" alt="..."  style={{width: '366px', height: '200px'}} />
                          <div className="card-body">
                            <Link href="/blog/item">
                              <a href="/blog/item" className="plain-link">
                                <h5 className="card-title">In the near future, when communications go offline.</h5>
                              </a>
                            </Link>
                            <p>
                            Two overly imaginative pranksters named George and Harold hypnotize their principal into thinking he’s a ridiculously enthusiastic, incredibly dimwitted.
                            </p>
                          </div>
                        </div>
                      </Fade>


                      <Fade>
                        <div className="text-left article">
                          <img src="/img/saitama.jpg" className="card-img-top" alt="..."  style={{width: '366px', height: '200px'}} />
                          <div className="card-body">
                            <Link href="/blog/item">
                              <a href="/blog/item" className="plain-link">
                                <h5 className="card-title">In the near future, when communications go offline.</h5>
                              </a>
                            </Link>
                            <p>
                            Two overly imaginative pranksters named George and Harold hypnotize their principal into thinking he’s a ridiculously enthusiastic, incredibly dimwitted.
                            </p>
                          </div>
                        </div>
                      </Fade>


                      <Fade>
                        <div className="text-left article">
                          <img src="/img/saitama.jpg" className="card-img-top" alt="..."  style={{width: '366px', height: '200px'}} />
                          <div className="card-body">
                            <Link href="/blog/item">
                              <a href="/blog/item" className="plain-link">
                                <h5 className="card-title">In the near future, when communications go offline.</h5>
                              </a>
                            </Link>
                            <p>
                            Two overly imaginative pranksters named George and Harold hypnotize their principal into thinking he’s a ridiculously enthusiastic, incredibly dimwitted.
                            </p>
                          </div>
                        </div>
                      </Fade>


                      <Fade>
                        <div className="text-left article">
                          <img src="/img/saitama.jpg" className="card-img-top" alt="..."  style={{width: '366px', height: '200px'}} />
                          <div className="card-body">
                            <Link href="/blog/item">
                              <a href="/blog/item" className="plain-link">
                                <h5 className="card-title">In the near future, when communications go offline.</h5>
                              </a>
                            </Link>
                            <p>
                            Two overly imaginative pranksters named George and Harold hypnotize their principal into thinking he’s a ridiculously enthusiastic, incredibly dimwitted.
                            </p>
                          </div>
                        </div>
                      </Fade>


                      <Fade>
                        <div className="text-left article">
                          <img src="/img/saitama.jpg" className="card-img-top" alt="..."  style={{width: '366px', height: '200px'}} />
                          <div className="card-body">
                            <Link href="/blog/item">
                              <a href="/blog/item" className="plain-link">
                                <h5 className="card-title">In the near future, when communications go offline.</h5>
                              </a>
                            </Link>
                            <p>
                            Two overly imaginative pranksters named George and Harold hypnotize their principal into thinking he’s a ridiculously enthusiastic, incredibly dimwitted.
                            </p>
                          </div>
                        </div>
                      </Fade>


                      <Fade>
                        <div className="text-left article">
                          <img src="/img/saitama.jpg" className="card-img-top" alt="..."  style={{width: '366px', height: '200px'}} />
                          <div className="card-body">
                            <Link href="/blog/item">
                              <a href="/blog/item" className="plain-link">
                                <h5 className="card-title">In the near future, when communications go offline.</h5>
                              </a>
                            </Link>
                            <p>
                            Two overly imaginative pranksters named George and Harold hypnotize their principal into thinking he’s a ridiculously enthusiastic, incredibly dimwitted.
                            </p>
                          </div>
                        </div>
                      </Fade>


                      <Fade>
                        <div className="text-left article">
                          <img src="/img/saitama.jpg" className="card-img-top" alt="..."  style={{width: '366px', height: '200px'}} />
                          <div className="card-body">
                            <Link href="/blog/item">
                              <a href="/blog/item" className="plain-link">
                                <h5 className="card-title">In the near future, when communications go offline.</h5>
                              </a>
                            </Link>
                            <p>
                            Two overly imaginative pranksters named George and Harold hypnotize their principal into thinking he’s a ridiculously enthusiastic, incredibly dimwitted.
                            </p>
                          </div>
                        </div>
                      </Fade>
                    </div>
                    :
                    <div className="card-columns">
                      {
                        props.BlogHomeRecent.map(item => 
                          <Fade>
                            <div className="text-left article" style={{height: '400px'}}>
                              <img src={process.env.MEDIA_URL+ `` + item.fields.feature_image} alt={item.fields.title} className="card-img-top"  style={{width: '366px', height: '200px'}} />
                              <div className="card-body" style={{width: '366.66px'}}>
                                <Link href={`/blog/post/` + item.fields.slug}>
                                  <a href={`/blog/post/` + item.fields.slug} className="plain-link">
                                    <h5 className="card-title">{item.fields.title}.</h5>
                                  </a>
                                </Link>
                              </div>
                            </div>
                          </Fade>
                        )
                      }
                    </div>
                  }
                </div>
              </div>
            </div>
          </LightSpeed>

          <Bounce delay={600}>
            <section className="section_gap newsletter-area" style={{backgroundImage: "url(/img/bg2.png)" }}>
              <div className="container">
                <div className="row justify-content-center">
                  <div className="col-lg-6 text-center">
                    <div className="main-title white-shadow">
                      <h1>Join My Newsletter</h1>
                      <p>Subscribe to get my latest blog post, project etc..</p>
                    </div>
                  </div>
                </div>
                <div className="row newsletter_form justify-content-center">
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <div className="d-flex flex-row" id="mc_embed_signup">
                      <form className="w-100" noValidate={true} action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&id=92a4423d01" method="get">
                        <div className="navbar-form">
                          <div className="input-group add-on">
                            <input className="form-control" name="EMAIL" placeholder="Your email address"  required type="email" />
                            <div style={{position: 'absolute', left: '-5000px'}}>
                              <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabIndex={-1} type="text" />
                            </div>
                            <div className="input-group-btn">
                              <button className="genric-btn text-uppercase">
                                Subscribe
                              </button>
                            </div>
                          </div>
                        </div>
                        <div className="info mt-20" />
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </Bounce>

        </>
  )
}

export const getServerSideProps = async () => {

  const res = await fetch(`${process.env.API_URL}work-home`)
  const WorkHome = await res.json()

  const res1 = await fetch(`${process.env.API_URL}aboutInfo`)
  const AboutInfo = await res1.json()

  const res3 = await fetch(`${process.env.API_URL}blog-home-recent`)
  const BlogHomeRecent = await res3.json()

  const res4 = await fetch(`${process.env.API_URL}tutorial-home-recent`)
  const TutorialHomeRecent = await res4.json()

  return {
    props: {
      WorkHome,
      AboutInfo,
      BlogHomeRecent,
      TutorialHomeRecent
    },
  }
}

export default Home