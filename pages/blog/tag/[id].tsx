import Head from 'next/head'
import fetch from 'node-fetch'
import Link from 'next/link'
import { LightSpeed } from 'react-reveal'
import Pagination from '../../../global/pagination'
import Bounce from 'react-reveal/Bounce'
import Moment from 'react-moment'
import 'moment-timezone'
import Tags from '../../../components/blog/tags'
import Categories from '../../../components/blog/categories'
import Sidebar from '../../../components/blog/sidebar'

export const Tag = (props) => {
  return (
    <div style={{marginTop: '100px'}}>
        <Head>
            <title>Ryan Dingle | Blog Tags</title>

            <meta property="og:title" content="Ryan Dingle | Blog Tag"  />
            <meta property="og:type" content="article" />
            <meta property="og:description" content="Ryan Dingle Blog Tag Page" />
            <meta property="og:image" content={ `${process.env.MEDIA_URL}${props.Site[0]?.fields.image}` } />
            <meta property="og:url" content={ process.env.APP_URL } />
            <meta name="twitter:card" content={ `${process.env.MEDIA_URL}${props.Site[0]?.fields.image}` } />
    
            <meta property="og:site_name" content="Ryan Dingle Blog Tag Page" />
            <meta name="twitter:image:alt" content="Ryan Dingle Blog Tag Page" />
        </Head>

        <section className="blog_area mt-s2">
          <div className="container">

              <div className="row">
                  <div className="col-lg-8">
                      <div className="blog_left_sidebar">
                        <div>
                          { 
                            props.list.length === 0  ? 
                              <div className="mt-s text-center">
                                  <h3>No Articles Found.</h3>
                              </div>
                            : 
                            props.list.map(post => 
                              <Bounce>
                                  <article className="row blog_item">
                                      <div className="col-md-3">
                                          <div className="blog_info text-right">
                                              <div className="post_tag">
                                                  <Categories isRefer={true} cat={post.fields.category} data={props.categories} />
                                                  <Tags isRefer={true} items={post.fields.tag} data={props.tags} />
                                              </div>
                                              <ul className="blog_meta list">
                                                  <li>
                                                    <Link href={`/about`}>
                                                      <a>Ryan Dingle<i className="lnr lnr-user"  /></a>
                                                    </Link>
                                                  </li>
                                                  <li>
                                                    <Link href={`/blog/post/${post.fields.slug}`}>
                                                      <a><Moment toNow>{post.fields.published_date}</Moment><i className="lnr lnr-calendar-full" /></a>
                                                    </Link>
                                                  </li>
                                              </ul>
                                          </div>
                                      </div>
                                      <div className="col-md-9">
                                          <div className="blog_post">
                                              <img src={`${process.env.MEDIA_URL}${post.fields.feature_image}`} alt={post.fields.title} />
                                              <div className="blog_details">
                                                  <Link href={`/blog/post/${post.fields.slug}`}>
                                                    <a><h2>{post.fields.title}</h2></a>
                                                  </Link>
                                                  <div> { post.fields.body.substr(0,500).replace(/<[^>]+>/gm, '') } ... </div>
                                                  <br />
                                                  <Link href={`/blog/post/${post.fields.slug}`}>
                                                    <a className="primary-btn">Read More<i className="fa fa-link" /></a>
                                                  </Link>
                                                  <hr />
                                              </div>
                                          </div>
                                      </div>
                                  </article>
                              </Bounce>
                              )
                            }

                            <hr />

                            <LightSpeed bottom>
                              <Pagination Hostname={process.env.APP_URL} path={`blog/tag/${props.id}`} pages={props.pages} active={props.active}  />
                            </LightSpeed>
                        </div>
                      </div>
                  </div>

                  <Sidebar item={false} recents={props.recents} categories={props.categories} tags={props.tags} search_input={props.search_input} />
              </div>
          </div>
        </section>
        );
    </div>
  )
}

export const getServerSideProps = async (context) => {

  const id = context.params.id ? context.params.id : ''
  
  const page = context.query.page != undefined ? `&page=${context.query.page}` : `&page=1`
  const active = context.query.page != undefined ? context.query.page : 1

  let res = await fetch(`${process.env.API_URL}blog-tag?item=${id}${page}`)
  const data = await res.json()

  const pages = data.pages
  const list = data.data

  const tagr = await fetch(`${process.env.API_URL}tag`)
  const tag = await tagr.json()
  const tags = tag

  const categoryr = await fetch(`${process.env.API_URL}category`)
  const category = await categoryr.json()
  const categories = category

  const recentr = await fetch(`${process.env.API_URL}blog-recent`)
  const recent = await recentr.json()
  const recents = recent

  return { 
    props:{
      list, pages, active, categories, tags, recents, id
    }
  }
}

export default Tag