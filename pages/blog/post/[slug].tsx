import Head from 'next/head'
import Link from 'next/link'
import fetch from 'node-fetch'
import { LightSpeed } from 'react-reveal'
import Zoom from 'react-reveal/Zoom'
import Bounce from 'react-reveal/Bounce'
import ReactHtmlParser from 'react-html-parser'
import Moment from 'react-moment'
import 'moment-timezone'
import Preloader from '../../../global/preloader'
import { useRouter } from 'next/router'
import DisqusComments from '../../../global/comment'
import Sidebar from '../../../components/blog/sidebar'
import Tags from '../../../components/blog/tags'
import Categories from '../../../components/blog/categories'
import {
  FacebookIcon,
  FacebookShareButton,
  TwitterIcon,
  TwitterShareButton,
  LinkedinShareButton,
  LinkedinIcon,
  PinterestShareButton,
  PinterestIcon,
  EmailShareButton,
  EmailIcon,
  RedditIcon,
  RedditShareButton
} from "react-share"

export const Post = (props) => {
  const router = useRouter()

  // If the page is not yet generated, this will be displayed
  // initially until getStaticProps() finishes running
  if (router.isFallback) {
    return <Preloader />
  }

  return (
    <>
        <Head>
            <title>{props.item.fields.title}</title>

            <meta property="og:title" content={props.item.fields.title}  />
            <meta property="og:type" content="article" />
            <meta property="og:description" content={ props.item.fields.body.substr(0,500).replace(/<[^>]+>/gm, '') } />
            <meta property="og:image" content={process.env.MEDIA_URL+``+props.item.fields.feature_image} />
            <meta property="og:url" content={`${process.env.APP_URL}blog/post/${props.item.fields.slug}`} />
            <meta name="twitter:card" content={process.env.MEDIA_URL+``+props.item.fields.feature_image} />
            <meta property="og:site_name" content={props.item.fields.title} />
            <meta name="twitter:image:alt" content={props.item.fields.title} />

        </Head>

        <section className="blog_area single-post-area section_gap">
            <div className="container">
                <div className="row">
                    <div className="col-lg-8 posts-list">
                        <div className="single-post row">
                            <div className="col-lg-12">
                                <Bounce top>
                                    <div className="feature-img">
                                        <img className="img-fluid" style={{width: '100%' }} src={process.env.MEDIA_URL+``+props.item.fields.feature_image} alt={props.item.fields.title} />
                                    </div>
                                </Bounce>
                            </div>
                            <div className="col-lg-3  col-md-3">
                                
                            </div>
                            <div className="col-lg-12 col-md-12 blog_details">
                                <Zoom left>
                                    <h1>{props.item.fields.title}</h1>
                                </Zoom>

                                <hr />

                                <div className="blog_info text-left">
                                    <div className="row">
                                        <div className="col-md-6">
                                            <LightSpeed left>
                                                <div className="post_tag">
                                                    <Categories isRefer={true} cat={props.item.fields.category} data={props.categories} />
                                                    <Tags isRefer={true} items={props.item.fields.tag} data={props.tags}  />
                                                </div>
                                            </LightSpeed>
                                        </div>
                                        <div className="col-md-3">
                                            <LightSpeed left delay={200}>
                                                <ul className="blog_meta list">
                                                    <li>
                                                      <Link href="/about">
                                                        <a>Ryan Dingle<i className="lnr lnr-user" /></a>
                                                      </Link>
                                                    </li>
                                                    <li>
                                                      <Link href={`/blog?item=`+props.item.fields.slug}>
                                                        <a><Moment toNow>{props.item.fields.published_date}</Moment><i className="lnr lnr-calendar-full" /></a>
                                                      </Link>
                                                    </li>
                                                </ul>
                                            </LightSpeed>
                                        </div>
                                        <div className="col-md-3">
                                            <LightSpeed left delay={400}>
                                                <ul className="social-links">
                                                    <li>
                                                        <div className="Demo__some-network">
                                                            <FacebookShareButton
                                                                url={`${process.env.APP_URL}blog/post/${props.item.fields.slug}`}
                                                                quote={props.item.fields.title}
                                                                className="Demo__some-network__share-button"
                                                            >
                                                                <FacebookIcon size={32} round />
                                                            </FacebookShareButton>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div className="Demo__some-network">
                                                            <TwitterShareButton
                                                                url={`${process.env.APP_URL}blog/post/${props.item.fields.slug}`} 
                                                                title={`${props.item.fields.title}`}
                                                                className="Demo__some-network__share-button"
                                                            >
                                                                <TwitterIcon size={32} round />
                                                            </TwitterShareButton>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div className="Demo__some-network">
                                                            <LinkedinShareButton
                                                                url={`${process.env.APP_URL}blog/post/${props.item.fields.slug}`}
                                                                title={props.item.fields.title}
                                                                className="Demo__some-network__share-button"
                                                            >
                                                                <LinkedinIcon size={32} round />
                                                            </LinkedinShareButton>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div className="Demo__some-network">
                                                            <RedditShareButton
                                                                url={`${process.env.APP_URL}blog/post/${props.item.fields.slug}`}
                                                                title={props.item.fields.title}
                                                                className="Demo__some-network__share-button"
                                                            >
                                                                <RedditIcon size={32} round />
                                                            </RedditShareButton>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div className="Demo__some-network">
                                                            <PinterestShareButton
                                                                url={`${process.env.APP_URL}blog/post/${props.item.fields.slug}`}
                                                                media={process.env.MEDIA_URL+``+props.item.fields.feature_image}
                                                                className="Demo__some-network__share-button"
                                                            >
                                                                <PinterestIcon size={32} round />
                                                            </PinterestShareButton>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div className="Demo__some-network">
                                                            <EmailShareButton
                                                                url={`${process.env.APP_URL}blog/post/${props.item.fields.slug}`}
                                                                subject={`Ryan Dingle Post - ${props.item.fields.title}`}
                                                                body={props.item.fields.body}
                                                                className="Demo__some-network__share-button"
                                                            >
                                                                <EmailIcon size={32} round />
                                                            </EmailShareButton>
                                                        </div>
                                                    </li> 
                                                </ul>
                                            </LightSpeed>
                                        </div>
                                    </div>
                                </div>

                                <hr />
                                <div className="col-md-12">{ ReactHtmlParser (props.item.fields.body) }</div>
                            </div>
                        </div>

                        <div className="col-md-12">
                            <h3>Share Article</h3>
                            <ul className="social-links">
                                <li>
                                  <div className="Demo__some-network">
                                      <FacebookShareButton
                                          url={`${process.env.APP_URL}blog/post/${props.item.fields.slug}`}
                                          quote={props.item.fields.title}
                                          className="Demo__some-network__share-button"
                                      >
                                          <FacebookIcon size={32} round />
                                      </FacebookShareButton>
                                  </div>
                              </li>
                              <li>
                                  <div className="Demo__some-network">
                                      <TwitterShareButton
                                          url={`${process.env.APP_URL}blog/post/${props.item.fields.slug}`} 
                                          title={`${props.item.fields.title}`}
                                          className="Demo__some-network__share-button"
                                      >
                                          <TwitterIcon size={32} round />
                                      </TwitterShareButton>
                                  </div>
                              </li>
                              <li>
                                  <div className="Demo__some-network">
                                      <LinkedinShareButton
                                          url={`${process.env.APP_URL}blog/post/${props.item.fields.slug}`}
                                          title={props.item.fields.title}
                                          className="Demo__some-network__share-button"
                                      >
                                          <LinkedinIcon size={32} round />
                                      </LinkedinShareButton>
                                  </div>
                              </li>
                              <li>
                                  <div className="Demo__some-network">
                                      <RedditShareButton
                                          url={`${process.env.APP_URL}blog/post/${props.item.fields.slug}`}
                                          title={props.item.fields.title}
                                          className="Demo__some-network__share-button"
                                      >
                                          <RedditIcon size={32} round />
                                      </RedditShareButton>
                                  </div>
                              </li>
                              <li>
                                  <div className="Demo__some-network">
                                      <PinterestShareButton
                                          url={`${process.env.APP_URL}blog/post/${props.item.fields.slug}`}
                                          media={process.env.MEDIA_URL+``+props.item.fields.feature_image}
                                          className="Demo__some-network__share-button"
                                      >
                                          <PinterestIcon size={32} round />
                                      </PinterestShareButton>
                                  </div>
                              </li>
                              <li>
                                  <div className="Demo__some-network">
                                      <EmailShareButton
                                          url={`${process.env.APP_URL}blog/post/${props.item.fields.slug}`}
                                          subject={`Ryan Dingle Post - ${props.item.fields.title}`}
                                          body={props.item.fields.body}
                                          className="Demo__some-network__share-button"
                                      >
                                          <EmailIcon size={32} round />
                                      </EmailShareButton>
                                  </div>
                              </li> 
                            </ul>
                        </div>

                        <div className="navigation-area">
                            <div className="row">
                                {
                                    props.previous && !props.next ?
                                        <div className="col-lg-6 col-md-6 col-12 nav-right flex-row d-flex justify-content-end align-items-center">
                                        </div>
                                    : null
                                }

                                {
                                    props.previous ?
                                      <LightSpeed left>
                                          <div className="col-lg-6 col-md-6 col-12 nav-left flex-row d-flex justify-content-start align-items-center">
                                              <div className="thumb">
                                                <Link href={`/blog/post/${props.previous.fields.slug}`}>
                                                  <a><img className="img-fluid" style={{height:'60px', width: '60px'}}  src={process.env.MEDIA_URL+``+props.previous.fields.feature_image}  alt={props.previous.fields.title} /></a>
                                                </Link>
                                              </div>
                                              <div className="arrow">
                                                <Link href={`/blog/post/${props.previous.fields.slug}`}>
                                                  <a><span className="lnr text-white lnr-arrow-left" /></a>
                                                </Link>
                                              </div>
                                              <div className="detials">
                                              <p>Prev Post</p>
                                                <Link href={`/blog/post/${props.previous.fields.slug}`}>
                                                  <a><h4>{props.item.fields.title}</h4></a>
                                                </Link>
                                              </div>
                                          </div>
                                      </LightSpeed>
                                    : null
                                }

                                {
                                    props.next && !props.previous ?
                                        <div className="col-lg-6 col-md-6 col-12 nav-left flex-row d-flex justify-content-start align-items-center">
                                        </div>
                                    : null
                                }

                                {
                                    props.next ?
                                      <LightSpeed right>
                                          <div className="col-lg-6 col-md-6 col-12 nav-right flex-row d-flex justify-content-end align-items-center">
                                              <div className="details">
                                              <p>Next Post</p>
                                              <Link href={`/blog/post/${props.next.fields.slug}`} >
                                                <a>
                                                  <h4>{props.next.fields.title}</h4>
                                                </a>
                                              </Link>
                                              </div>
                                              <div className="arrow">
                                                  <Link href={`/blog/post/${props.next.fields.slug}`}>
                                                    <a><span className="lnr text-white lnr-arrow-right" /></a>
                                                  </Link>
                                              </div>
                                              <div className="thumb">
                                                <Link href={`/blog/post/${props.next.fields.slug}`}>
                                                  <a><img className="img-fluid" style={{height:'60px', width: '60px'}} src={process.env.MEDIA_URL+``+props.next.fields.feature_image}  alt={props.next.fields.title} /></a>
                                                </Link>
                                              </div>
                                          </div>
                                      </LightSpeed>
                                    : null
                                }
                            </div>
                        </div>
                            
                        <div className="col-md-12 pull-right">
                            <DisqusComments item={props.item} Hostname={process.env.APP_URL} />
                        </div>
                    </div>
                    
                    <Sidebar item={props.item.fields.slug} recents={props.recents} categories={props.categories} tags={props.tags} />
                
                </div>
            </div>
        </section>
    </>
  )
}

export const getStaticPaths = async () => {
    const res = await fetch(`${process.env.API_URL}blog`)
    const posts = await res.json()
    const posts_all = posts.data
    const paths = posts_all.map(post => `/blog/post/${post.fields.slug}`)

    return { paths, fallback: true }
}

export const getStaticProps = async (context) => {
    const query = context.params
    const res = await fetch(`${process.env.API_URL}blog?item=${query.slug}`)
    const post = await res.json()
    const item = post.data[0]
    const next = post.next ? post.next[0] : false
    const previous = post.previous ? post.previous[0] : false

    const tagr = await fetch(`${process.env.API_URL}tag`)
    const tag = await tagr.json()
    const tags = tag

    const categoryr = await fetch(`${process.env.API_URL}category`)
    const category = await categoryr.json()
    const categories = category

    const recentr = await fetch(`${process.env.API_URL}blog-recent?item=${query.slug}`)
    const recent = await recentr.json()
    const recents = recent

    return { 
        props: {
            item, next, previous, categories, tags, recents
        }
    }
}

export default Post
