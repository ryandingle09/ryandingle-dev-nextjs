import Head from 'next/head'
import fetch from 'node-fetch'
import { Fade, LightSpeed } from 'react-reveal'
import FormContact from '../components/contact/form'
import Ads from '../global/ads'

export const Contact = (props) => {
  return (
    <>
        <Head>
            <title>Ryan Dingle | Contact</title>

            <meta property="og:title" content="Ryan Dingle | Contact"  />
            <meta property="og:description" content="Ryan Dingle Contact Page" />
            <meta property="og:type" content="article" />
            <meta property="og:image" content={ `${process.env.MEDIA_URL}${props.Site[0]?.fields.image}` } />
            <meta property="og:url" content={ process.env.APP_URL } />
            <meta name="twitter:card" content={ `${process.env.MEDIA_URL}${props.Site[0]?.fields.image}` } />
    
            <meta property="og:site_name" content="Ryan Dingle Contact Page" />
            <meta name="twitter:image:alt" content="Ryan Dingle Contact Page" />
        </Head>

        <section className="contact_area section_gap">
            <div className="container">
                {
                  props.ContactInfo.length === 0 ?
                      <div className="row">
                          <div className="col-lg-3">
                              <LightSpeed left>
                                  <div className="contact_info">
                                      <div className="info_item">
                                      <i className="lnr lnr-home" />
                                      <h6>California, United States</h6>
                                      <p>Santa monica bullevard</p>
                                      </div>
                                      <div className="info_item">
                                      <i className="lnr lnr-phone-handset" />
                                      <h6><a href="/">00 (440) 9865 562</a></h6>
                                      <p>Mon to Fri 9am to 6 pm</p>
                                      </div>
                                      <div className="info_item">
                                      <i className="lnr lnr-envelope" />
                                      <h6><a href="/">me@rldwebshop.com</a></h6>
                                      <p>Send us your query anytime!</p>
                                      </div>
                                  </div>
                              </LightSpeed>
                          </div>
                          <div className="col-lg-9 mb-s">
                              <FormContact />
                          </div>
                      </div>
                  :
                    props.ContactInfo.map(item => 
                      <div className="row">
                          <div className="col-lg-12">
                              <LightSpeed left><h3>Contact Me</h3></LightSpeed>
                              <br />
                              <Fade right><p>{item.fields.description}</p></Fade>
                              <br />
                              <br />
                          </div>
                          <hr />
                          <div className="col-lg-3">
                              <LightSpeed left>
                                  <div className="contact_info">
                                      <div className="info_item">
                                      <i className="lnr lnr-home" />
                                      <h6>{item.fields.address}</h6>
                                      <p>&nbsp;</p>
                                      </div>
                                      <div className="info_item">
                                      <i className="lnr lnr-phone-handset" />
                                      <h6><a href="/">{item.fields.contact}</a></h6>
                                      <p>Mon to Fri 8am to 10pm</p>
                                      </div>
                                      <div className="info_item">
                                      <i className="lnr lnr-envelope" />
                                      <h6><a href="/">{item.fields.email}</a></h6>
                                      <p>Send us your query anytime!</p>
                                      </div>
                                  </div>
                              </LightSpeed>
                          </div>
                          <div className="col-lg-9 mb-s">
                              <FormContact />
                          </div>
                      </div>
                  )
                }
                <div className="col-md-12">
                    <Ads />
                </div>
            </div>
        </section>
    </>
  )
}

export const getServerSideProps = async () => {
  const res1 = await fetch(`${process.env.API_URL}contactInfo`)
  const ContactInfo = await res1.json()

  return {
    props: {
      ContactInfo,
    },
  }
}

export default Contact
