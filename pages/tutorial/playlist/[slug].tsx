import Head from 'next/head'
import fetch from 'node-fetch'
import Link from 'next/link'
import { LightSpeed } from 'react-reveal'
import Pagination from '../../../global/pagination'
import Bounce from 'react-reveal/Bounce'
import 'moment-timezone'
import Sidebar from '../../../components/tutorial/sidebar'

export const Playlist = (props) => {
  return (
    <div style={{marginTop: '100px'}}>
        <Head>
            <title>Ryan Dingle | Tutorial Playlist</title>

            <meta property="og:title" content="Ryan Dingle | Tutorial Playlist"  />
            <meta property="og:type" content="article" />
            <meta property="og:description" content="Ryan Dingle Tutorial Playlist Page" />
            <meta property="og:image" content={ `${process.env.MEDIA_URL}${props.Site[0]?.fields.image}` } />
            <meta property="og:url" content={ process.env.APP_URL } />
            <meta name="twitter:card" content={ `${process.env.MEDIA_URL}${props.Site[0]?.fields.image}` } />
    
            <meta property="og:site_name" content="Ryan Dingle Tutorial Playlist Page" />
            <meta name="twitter:image:alt" content="Ryan Dingle Tutorial Playlist Page" />
        </Head>

        <section className="blog_area mt-s2">
          <div className="container">

              <div className="row">
                  <div className="col-lg-8">
                      <div className="blog_left_sidebar">
                        <div>
                          { 
                            props.list.length === 0  ? 
                              <div className="mt-s text-center">
                                  <h3>No Tutorials Found.</h3>
                              </div>
                            : 
                            props.list.map(post => 
                              <Bounce>
                                  <article className="row blog_item">
                                      <div className="col-md-12">
                                          <div className="blog_post">
                                              <img src={`${process.env.MEDIA_URL}${post.fields.feature_image}`} alt={post.fields.title} />
                                              <div className="blog_details text-center">
                                                  <Link href={`/tutorial/series/${post.fields.slug}`}>
                                                    <a><h2>{post.fields.title}</h2></a>
                                                  </Link>
                                                  <hr />
                                              </div>
                                          </div>
                                      </div>
                                  </article>
                              </Bounce>
                              )
                            }

                            <hr />

                            <LightSpeed bottom>
                              <Pagination Hostname={process.env.APP_URL} path='tutorial' pages={props.pages} active={props.active}  />
                            </LightSpeed>
                        </div>
                      </div>
                  </div>

                  <Sidebar item={false} recents={props.recents} categories={props.categories} />
              </div>
          </div>
        </section>
        );
    </div>
  )
}

export const getServerSideProps = async (context) => {

  const id = context.params.slug ? context.params.slug : ''
  
  const page = context.query.page != undefined ? `&page=${context.query.page}` : `&page=1`
  const active = context.query.page != undefined ? context.query.page : 1

  let res = await fetch(`${process.env.API_URL}tutorial-playlist?item=${id}${page}`)
  const data = await res.json()

  const pages = data.pages
  const list = data.data

  const categoryr = await fetch(`${process.env.API_URL}playlist`)
  const category = await categoryr.json()
  const categories = category

  const recentr = await fetch(`${process.env.API_URL}tutorial-recent`)
  const recent = await recentr.json()
  const recents = recent

  return { 
    props: {
      list, pages, active, categories, recents, id
    }
  }
}

export default Playlist