import Head from 'next/head'
import fetch from 'node-fetch'
import { LightSpeed, Fade } from 'react-reveal'
import { VerticalTimeline, VerticalTimelineElement }  from 'react-vertical-timeline-component'

export const About = (props) => {
  return (
    <>
        <Head>
            <title>Ryan Dingle | About</title>

            <meta property="og:title" content="Ryan Dingle | About"  />
            <meta property="og:description" content="Ryan Dingle About Page" />
            <meta property="og:type" content="article" />
            <meta property="og:image" content={ `${process.env.MEDIA_URL}${props.Site[0]?.fields.image}` } />
            <meta property="og:url" content={ process.env.APP_URL } />
            <meta name="twitter:card" content={ `${process.env.MEDIA_URL}${props.Site[0]?.fields.image}` } />
    
            <meta property="og:site_name" content="Ryan Dingle About Page" />
            <meta name="twitter:image:alt" content="Ryan Dingle About Page" />
        </Head>

        <section className="about-area section_gap gray-bg">
          {
            <div className="container">
                <div className="row align-items-center justify-content-between">

                {
                    props.AboutInfo.length === 0  ? 
                    <LightSpeed left>
                        <div className="col-lg-5 about-left">
                            <img className="img-fluid" src="img/about-img.png" alt="" />
                        </div>
                    </LightSpeed>
                    :
                    props.AboutInfo.map(item => 
                        <LightSpeed left>
                            <div className="col-lg-5 about-left">
                                <img className="img-fluid" src={process.env.MEDIA_URL+``+ item.fields.image } alt={item.heading} />
                            </div>
                        </LightSpeed>
                    )
                }

                {
                    props.AboutInfo.length === 0  ? 
                        <LightSpeed right>
                            <div className="col-lg-6 col-md-12 about-right">
                                <div className="main-title text-left">
                                    <h1>about myself</h1>
                                </div>
                                <div className="mb-50 wow fadeIn" data-wow-duration=".8s">
                                    <p>
                                    inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct standards
                                    especially in the
                                    workplace. That’s why it’s crucial that, as women, our behavior on the job is beyond reproach. inappropriate
                                    behavior
                                    is often laughed. inappropriate behavior is often laughed off as “boys will be boys,” women face higher.
                                    </p>
                                    <p>That’s why it’s crucial that, as women, our behavior on the job is beyond reproach. inappropriate behavior is
                                    often
                                    laughed.
                                    </p>
                                </div>
                            </div>
                        </LightSpeed>
                    : 
                    props.AboutInfo.map(item => 
                        <LightSpeed right>
                            <div className="col-lg-6 col-md-12 about-right">
                                <div className="main-title text-left">
                                    <h1>{item.fields.heading}</h1>
                                </div>
                                <div className="mb-50 wow fadeIn" data-wow-duration=".8s">
                                    <p>{item.fields.introduction}</p>
                                </div>
                            </div>
                        </LightSpeed>
                    )
                }
                    
                </div>
                
                <hr />

                <h1 className="text-center mt-s2 mb-s2">History</h1>

                <VerticalTimeline>
                    {
                        props.History.map(item => 
                            <VerticalTimelineElement
                                className="vertical-timeline-element--work"
                                date={item.fields.start +` - `+ item.fields.end}
                                iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
                            >
                                <h3 className="vertical-timeline-element-title">{item.fields.job_title}</h3>
                                <p>{item.fields.description}</p>
                            </VerticalTimelineElement>
                        )
                    }
                    <VerticalTimelineElement
                        iconStyle={{ background: 'rgb(16, 204, 82)', color: '#fff' }}
                    />
                </VerticalTimeline>
                
                <hr />
                
                {
                    props.AboutInfo.length === 0  ? 
                    <h1 className="text-center mt-s2 mb-s2">Technologies</h1>
                    :
                    props.AboutInfo.map(item => 
                        <h1 className="text-center mt-s2 mb-s2">{item.fields.heading_bottom}</h1>
                    )
                }
                
                {
                    props.Skill.length === 0  ? 
                    <div className="mt-s2">
                        <div className="row">
                            <div className="col-md-2 mb-s2">
                                <Fade><img className="img-fluid" alt="" src="/img/web-app.png" /></Fade>
                            </div>
                            <div className="col-md-2 mb-s2">
                                <Fade><img className="img-fluid" alt="" src="/img/web-app.png" /></Fade>
                            </div>
                            <div className="col-md-2 mb-s2">
                                <Fade><img className="img-fluid" alt="" src="/img/web-app.png" /></Fade>
                            </div>
                            <div className="col-md-2 mb-s2">
                                <Fade><img className="img-fluid" alt="" src="/img/web-app.png" /></Fade>
                            </div>
                            <div className="col-md-2 mb-s2">
                                <Fade><img className="img-fluid" alt="" src="/img/web-app.png" /></Fade>
                            </div>
                            <div className="col-md-2 mb-s2">
                                <Fade><img className="img-fluid" alt="" src="/img/web-app.png" /></Fade>
                            </div>
                            <div className="col-md-2 mb-s2">
                                <Fade><img className="img-fluid" alt="" src="/img/web-app.png" /></Fade>
                            </div>
                            <div className="col-md-2 mb-s2">
                                <Fade><img className="img-fluid" alt="" src="/img/web-app.png" /></Fade>
                            </div>
                            <div className="col-md-2 mb-s2">
                                <Fade><img className="img-fluid" alt="" src="/img/web-app.png" /></Fade>
                            </div>
                            <div className="col-md-2 mb-s2">
                                <Fade><img className="img-fluid" alt="" src="/img/web-app.png" /></Fade>
                            </div>
                            <div className="col-md-2 mb-s2">
                                <Fade><img className="img-fluid" alt="" src="/img/web-app.png" /></Fade>
                            </div>
                            <div className="col-md-2 mb-s2">
                                <Fade><img className="img-fluid" alt="" src="/img/web-app.png" /></Fade>
                            </div>
                            <div className="col-md-2 mb-s2">
                                <Fade><img className="img-fluid" alt="" src="/img/web-app.png" /></Fade>
                            </div>
                            <div className="col-md-2 mb-s2">
                                <Fade><img className="img-fluid" alt="" src="/img/web-app.png" /></Fade>
                            </div>
                            <div className="col-md-2 mb-s2">
                                <Fade><img className="img-fluid" alt="" src="/img/web-app.png" /></Fade>
                            </div>
                            <div className="col-md-2 mb-s2">
                                <Fade><img className="img-fluid" alt="" src="/img/web-app.png" /></Fade>
                            </div>
                            <div className="col-md-2 mb-s2">
                                <Fade><img className="img-fluid" alt="" src="/img/web-app.png" /></Fade>
                            </div>
                            <div className="col-md-2 mb-s2">
                                <Fade><img className="img-fluid" alt="" src="/img/web-app.png" /></Fade>
                            </div>
                        </div>
                    </div>
                    :
                        <div className="mt-s2">
                            <div className="row">
                                {
                                    props.Skill.map(item => 
                                        <div className="col-md-2 mb-s2">
                                            <Fade><img className="img-fluid" src={process.env.MEDIA_URL+``+ item.fields.image } alt={item.fields.title} style={{width: '160px', height: '160px'}} /></Fade>
                                        </div>
                                    )
                                } 
                            </div>
                        </div>

                }
            </div>
          }
      </section>
    </>
  )
}

export const getServerSideProps = async () => {
  const res = await fetch(`${process.env.API_URL}history`)
  const History = await res.json()
  const res1 = await fetch(`${process.env.API_URL}aboutInfo`)
  const AboutInfo = await res1.json()
  const res3 = await fetch(`${process.env.API_URL}skill`)
  const Skill = await res3.json()

  return {
    props: {
      History,
      AboutInfo,
      Skill
    },
  }
}

export default About