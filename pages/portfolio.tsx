import Head from 'next/head'
import Link from 'next/link'
import fetch from 'node-fetch'
import { Zoom, Flip, LightSpeed, Fade } from 'react-reveal'
import Pagination from '../global/pagination'

export const Portfolio = (props) => {
  return (
    <>
        <Head>
            <title>Ryan Dingle | Portfolio</title>

            <meta property="og:title" content="Ryan Dingle | Portfolio"  />
            <meta property="og:description" content="Ryan Dingle Portfolio Page" />
            <meta property="og:type" content="article" />
            <meta property="og:image" content={ `${process.env.MEDIA_URL}${props.Site[0]?.fields.image}` } />
            <meta property="og:url" content={ process.env.APP_URL } />
            <meta name="twitter:card" content={ `${process.env.MEDIA_URL}${props.Site[0]?.fields.image}` } />
    
            <meta property="og:site_name" content="Ryan Dingle Portfolio Page" />
            <meta name="twitter:image:alt" content="Ryan Dingle Portfolio Page" />
        </Head>

        <section className="section_gap portfolio_area" id="work">
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-lg-6 text-center">
                    <div className="main-title">
                        <LightSpeed><h1>Portfolio</h1></LightSpeed>
                        <Fade><p>Check some of my works</p></Fade>
                    </div>
                    </div>
                </div>
                
                <div className="row justify-content-center">
                    {
                    props.portfolio.length === 0 ?
                    <div className="mt-s text-center">
                        <h3>No Projects Found.</h3>
                    </div>
                    :
                    <div>
                        <div className="card-columns">
                        {
                            props.portfolio.map(item => 
                            <Zoom delay="100">
                              <div className="card bg-light text-white items" data-item={item.pk}>
                                <Link href={`/portfolio/${item.pk}`}>
                                  <a className="plain-link2">
                                    <img src={process.env.MEDIA_URL+ `` + item.fields.cover_image} className="card-img-top" alt={item.fields.title}  style={{width: '366px', height: '366px'}} />
                                    <div className="card-img-overlay text-center">
                                        <div>
                                          <Flip bottom cascade>
                                            <Link href={`/portfolio/${item.pk}`}>
                                              <a className="primary-btn white-color white-shadow" style={{color:'white', marginTop: '40%'}}>
                                                View
                                              </a>
                                            </Link>
                                          </Flip>
                                        </div>
                                    </div>
                                  </a>
                                </Link>
                              </div>
                            </Zoom>
                            )
                        }
                        </div>

                        <hr />

                        <LightSpeed bottom>
                            <Pagination Hostname={process.env.APP_URL} path='portfolio' pages={props.pages} active={props.active}  />
                        </LightSpeed>
                    </div>
                    }
                </div>
            </div>
        </section>
    </>
  )
}

export const getServerSideProps = async (context) => {
  const query = context.query
  
  const page = query.page !== undefined ? `?page=${query.page}` : `?page=1`
  const active = query.page !== undefined ? query.page : 1

  const res = await fetch(`${process.env.API_URL}work${page}`)
  const data = await res.json()
  const pages = data.pages
  const portfolio = data.data

  return { 
    props: {
      portfolio,
      pages,
      active
    }
  }
}

export default Portfolio
