import fetch from 'node-fetch'
import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/global.css'
import 'react-vertical-timeline-component/style.min.css'
import 'react-toastify/dist/ReactToastify.min.css'
import Preloader2 from '../global/preloader2'
import Router from "next/router"
import { initGA, logPageView } from '../global/analytics'
import Layout from '../global/layout'

export const App = ({ Component, pageProps, Social, Site, Hostname }) => {

  const [loading, setLoading] = React.useState(false);
  
  React.useEffect(() => {
    initGA()
    logPageView()

    const start = () => {
      console.log("start");
      setLoading(true);

      initGA()
      logPageView()
    };
    const end = () => {
      console.log("finished");
      setLoading(false);
    };
    Router.events.on("routeChangeStart", start);
    Router.events.on("routeChangeComplete", end);
    Router.events.on('routeChangeComplete', logPageView)
    Router.events.on("routeChangeError", end);
    return () => {
      Router.events.off("routeChangeStart", start);
      Router.events.off("routeChangeComplete", end);
      Router.events.off("routeChangeError", end);
    };
  }, []);
  
  return (
    <>
        <Layout {...pageProps} Social={Social} Site={Site}>
          {
            loading ?
            <Preloader2 />
          : 
          <Component {...pageProps} Social={Social} Site={Site} Hostname={Hostname} />
          }
        </Layout>
    </>
  );
}

App.getInitialProps = async ({ Component, ctx }) => {

  let pageProps = {}

  if (Component.getInitialProps) {
    pageProps = await Component.getInitialProps(ctx)
  }

  const res = await fetch(`${process.env.API_URL}site`)
  const Site = await res.json()

  const res2 = await fetch(`${process.env.API_URL}social`)
  const Social = await res2.json()

  const Hostname = ctx.req ? ctx.req.headers.host : ''

  return { Social, Site, Hostname }
}

export default App