import Head from 'next/head'
import { LightSpeed, Fade } from 'react-reveal'
import Rotate from 'react-reveal/Rotate'
import ReactHtmlParser from 'react-html-parser'
import fetch from 'node-fetch'
import Preloader from '../../global/preloader'
import { useRouter } from 'next/router'
import {
    FacebookIcon,
    FacebookShareButton,
    TwitterIcon,
    TwitterShareButton,
    LinkedinShareButton,
    LinkedinIcon,
    PinterestShareButton,
    PinterestIcon,
    EmailShareButton,
    EmailIcon,
    RedditIcon,
    RedditShareButton
  } from "react-share"

export const Item = (props) => {
  const router = useRouter()

  // If the page is not yet generated, this will be displayed
  // initially until getStaticProps() finishes running
  if (router.isFallback) {
    return <Preloader />
  }

  return (
    <>
        <Head>
            <title>{props.item.fields.title}</title>

            <meta property="og:title" content={props.item.fields.title}  />
            <meta property="og:type" content="article" />
            <meta property="og:description" content={props.item.fields.description.replace(/<[^>]+>/gm, '')} />
            <meta property="og:image" content={process.env.MEDIA_URL+``+props.item.fields.cover_image} />
            <meta property="og:url" content={`${process.env.APP_URL}portfolio/${props.item.pk}`} />
            <meta name="twitter:card" content={process.env.MEDIA_URL+``+props.item.fields.cover_image} />
            <meta property="og:site_name" content={props.item.fields.title} />  
            <meta name="twitter:image:alt" content={props.item.fields.title} />
        </Head>

        <section className="portfolio_details_area section_gap">
          <div className="container">
              <div className="portfolio_details_inner">
                  <div className="row">
                      <Rotate>
                          <div className="col-md-6">
                              <div className="left_img">
                                  <img className="img-fluid" src={process.env.MEDIA_URL+``+props.item.fields.cover_image} alt={props.item.fields.title} style={{width: '555px', height: '471px'}} />
                              </div>
                          </div>
                      </Rotate>

                      <div className="offset-md-1 col-md-5">
                          <LightSpeed  right>
                              <div className="portfolio_right_text mt-30">
                                  <h4>{props.item.fields.title}</h4>
                                  <ul className="list">
                                      <li><span>Client</span>: {props.item.fields.client}</li>
                                      <li>
                                          <span>Website</span>: &nbsp;
                                          {
                                              props.item.fields.link ? 
                                              <a href={props.item.fields.link}>{props.item.fields.link}</a>
                                              : 'N/A'
                                          }
                                      </li>
                                      <li><span>Completed</span>: {props.item.fields.date}</li>
                                  </ul>
                                  <ul className="list social_details">
                                  <li>
                                        <div className="Demo__some-network">
                                            <FacebookShareButton
                                                url={`${process.env.APP_URL}portfolio/${props.item.pk}`}
                                                quote={props.item.fields.title}
                                                className="Demo__some-network__share-button"
                                            >
                                                <FacebookIcon size={32} round />
                                            </FacebookShareButton>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="Demo__some-network">
                                            <TwitterShareButton
                                                url={`${process.env.APP_URL}portfolio/${props.item.pk}`} 
                                                title={`${props.item.fields.title}`}
                                                className="Demo__some-network__share-button"
                                            >
                                                <TwitterIcon size={32} round />
                                            </TwitterShareButton>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="Demo__some-network">
                                            <LinkedinShareButton
                                                url={`${process.env.APP_URL}portfolio/${props.item.pk}`}
                                                title={props.item.fields.title}
                                                className="Demo__some-network__share-button"
                                            >
                                                <LinkedinIcon size={32} round />
                                            </LinkedinShareButton>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="Demo__some-network">
                                            <RedditShareButton
                                                url={`${process.env.APP_URL}portfolio/${props.item.pk}`}
                                                title={props.item.fields.title}
                                                className="Demo__some-network__share-button"
                                            >
                                                <RedditIcon size={32} round />
                                            </RedditShareButton>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="Demo__some-network">
                                            <PinterestShareButton
                                                url={`${process.env.APP_URL}portfolio/${props.item.pk}`}
                                                media={process.env.MEDIA_URL+``+props.item.fields.feature_image}
                                                className="Demo__some-network__share-button"
                                            >
                                                <PinterestIcon size={32} round />
                                            </PinterestShareButton>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="Demo__some-network">
                                            <EmailShareButton
                                                url={`${process.env.APP_URL}portfolio/${props.item.pk}`}
                                                subject={`Ryan Dingle Post - ${props.item.fields.title}`}
                                                body={props.item.fields.body}
                                                className="Demo__some-network__share-button"
                                            >
                                                <EmailIcon size={32} round />
                                            </EmailShareButton>
                                        </div>
                                    </li> 
                                  </ul>
                              </div>
                          </LightSpeed>
                      </div>
                  </div>
                  <Fade bottom>
                      <div className="row">{ ReactHtmlParser (props.item.fields.description) }</div>
                  </Fade>
              </div>
          </div>
      </section>
    </>
  )
}

export const getStaticPaths = async () => {
    const res = await fetch(`${process.env.API_URL}work`)
    const posts = await res.json()
    const posts_all = posts.data
    const paths = posts_all.map(post => `/portfolio/${post.pk}`)

    return { paths, fallback: true }
}

export const getStaticProps = async (context) => {
    const query = context.params
    const res = await fetch(`${process.env.API_URL}work?item=${query.id}`)
    const post = await res.json()
    const item = post.data[0]
  
    return { props: { item } } 
}

export default Item
