const {
    PHASE_DEVELOPMENT_SERVER,
    PHASE_PRODUCTION_BUILD,
  } = require('next/constants')
  
  // This uses phases as outlined here: https://nextjs.org/docs/#custom-configuration
  module.exports = phase => {
    // when started in development mode `next dev` or `npm run dev` regardless of the value of STAGING environmental variable
    const isDev = phase === PHASE_DEVELOPMENT_SERVER
    // when `next build` or `npm run build` is used
    const isProd = phase === PHASE_PRODUCTION_BUILD && process.env.STAGING !== '1'
    // when `next build` or `npm run build` is used
    const isStaging =
      phase === PHASE_PRODUCTION_BUILD && process.env.STAGING === '1'
  
    console.log(`isDev:${isDev}  isProd:${isProd}   isStaging:${isStaging}`)
  
    const env = {
        APP_URL: (() => {
            if (isDev) return 'http://localhost:3000/'
            if (isProd) {
                return 'https://ryandingle.me/'
            }
            if (isStaging) return 'https://ryandingle.me/'
                return 'APP_URL:not (isDev,isProd && !isStaging,isProd && isStaging)'
        })(),
        API_URL: (() => {
            if (isDev) return 'http://localhost:8000/api/v1/'
            if (isProd) {
                // use if different server 
                return 'https://api.ryandingle.me/api/v1/'
                // return 'http://localhost:8000/api/v1/'
            }
            if (isStaging) return 'https://api.ryandingle.me/api/v1/'
                return 'API_URL:not (isDev,isProd && !isStaging,isProd && isStaging)'
        })(),
        MEDIA_URL: (() => {
            if (isDev) return 'https://ryandingle.s3.us-east-2.amazonaws.com/media/'
            if (isProd) return 'https://ryandingle.s3.us-east-2.amazonaws.com/media/'
            if (isStaging) return 'https://ryandingle.s3.us-east-2.amazonaws.com/media/'
                return 'MEDIA_URL:not (isDev,isProd && !isStaging,isProd && isStaging)'
        })(),
        DISQUS_NAMESPACE: (() => {
            if (isDev) return 'ryandingle-dev'
            if (isProd) return 'ryandingle-dev'
            if (isStaging) return 'ryandingle-dev'
                return 'DISQUS_NAMESPACE:not (isDev,isProd && !isStaging,isProd && isStaging)'
        })(),
        ANALYTICS_ID: (() => {
            if (isDev) return 'UA-166986505-1'
            if (isProd) return 'UA-166986505-1'
            if (isStaging) return 'UA-166986505-1'
                return 'ANALYTICS_ID:not (isDev,isProd && !isStaging,isProd && isStaging)'
        })(),
        ADSENSE_ID: (() => {
            if (isDev) return 'ca-pub-2121261107511559'
            if (isProd) return 'ca-pub-2121261107511559'
            if (isStaging) return 'ca-pub-2121261107511559'
                return 'ADSENSE_ID:not (isDev,isProd && !isStaging,isProd && isStaging)'
        })(),
    }
  
    // next.config.js object
    
    return {
        env,
        devIndicators: {
            autoPrerender: false,
        }
    }
  }